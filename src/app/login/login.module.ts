import { NgModule, NO_ERRORS_SCHEMA, InjectionToken } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { LoginRoutingModule } from "./login-routing.module";
import { LoginComponent } from "./components/login.component";
 


@NgModule({
    imports: [
        LoginRoutingModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        LoginComponent
    ],
  
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class LoginModule { }
