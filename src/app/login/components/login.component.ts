import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { User } from "~/app/models/user.model";
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { HttpClient } from "@angular/common/http";
import { TextField } from "tns-core-modules/ui/text-field";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { RouterExtensions } from "nativescript-angular/router";
import { Values } from "~/app/values/values";

@Component({
  selector: "app-login",
  moduleId: module.id,
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {

  user: User;
  emailText: string = "";
  passwordText: string = "";
  routerExtensions: RouterExtensions;
  //   http: HttpClient;
  constructor(
    private page: Page,
    private http: HttpClient,
    routerExtensions: RouterExtensions
    // private router: Router
  ) {
    this.page.actionBarHidden = true;
    this.user = new User();
    this.routerExtensions = routerExtensions;
    // this.http = http;
  }

  public emailTextField(args) {
    var textField = <TextField>args.object;
    this.emailText = textField.text;
  }

  public passwordTextField(args) {
    var textField = <TextField>args.object;
    this.passwordText = textField.text;
  }

  // onButtonClick() {
  //   this.routerExtensions.navigate(["/dashboard"]);
  // }

  ngOnInit(): void {
    this.onLoginTap();
  }

  onLoginTap() {

    //UNCOMMENT FOR DYNAMIC DATA

    // if (this.emailText == "") {
    //   alert("email cannot be empty");
    //   return;
    // }
    // if (this.passwordText == "") {
    //   alert("password cannot be empty");
    //   return;
    // }

    // this.user.email = this.emailText;
    // this.user.password = this.passwordText;

    //UNCOMMENT FOR DEVELOPMENT PURPOSES
    this.user.email = "sachindeep@prinweb.com";
    this.user.password = "password"

    this.http
      .post(Values.BASE_URL + "/api/login", this.user)
      .subscribe((res: any) => {
        if (!(res.access_token == "")) {
          let autherization_token: any;
          autherization_token = res.access_token;
          //   alert("Your access token is: " + accessToken);

          Values.writeString(Values.AUTHERIZATION_TOKEN, autherization_token);
          console.trace(autherization_token);
          // dialogs
          //   .confirm({
          //     title: "Your access token is: ",
          //     message: autherization_token,
          //     okButtonText: "OK"
          //   })
          //   .then(function (result) {
          //     if (result == true) {
          //       // alert("Login successfully");
          //       // this.routerExtensions.navigate(["/dashboard"]);
          //     }
          //   });
          this.routerExtensions.navigate(["/dashboard"]);
        } else {
          // alert(res.error);
          console.trace(res.error)
        }
      },
        error => {
          alert(error);
        });
  }
}
