import * as appSettings from "application-settings"

export class Values {

    public static AUTHERIZATION_TOKEN: string = "autherization_token"
    public static BASE_URL: string = "http://pf.prinweb.com"


    public static writeString(key: string, value: string): void {
        appSettings.setString(key, value);
    }

    public static readString(key: string, defaultValue: string): string {
        return appSettings.getString(key, defaultValue);
    }

    public static writeNumber(key: string, value: number): void {
        appSettings.setNumber(key, value);
    }

    public static readNumber(key: string, defaultValue: number): number {
        return appSettings.getNumber(key, defaultValue);
    }

    public static writeBoolean(key: string, value: boolean): void {
        appSettings.setBoolean(key, value);
    }

    public static readBoolean(key: string, defaultValue: boolean): boolean {
        return appSettings.getBoolean(key, defaultValue);
    }

    public static doesExist(key: string): boolean {
        return appSettings.hasKey(key);
    }

    public static remove(key: string) {
        appSettings.remove(key);
    }


}