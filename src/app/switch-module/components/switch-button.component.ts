import { Component, Input, Output, EventEmitter } from "@angular/core";
import { EventData } from "tns-core-modules/ui/page/page";

@Component({
    selector: "switch-button",
    moduleId: module.id,
    styleUrls: ["./switch-button.component.css"],
    templateUrl: "./switch-button.component.html",
})
export class SwitchButtonComponent {
    @Input() width: string = "125vh"
    @Input() height: string = "40vh"
    @Input() borderColor: string = "red"
    @Input() borderRadius: string = "5vh"
    @Input() borderWidth: string = "2px"
    @Input() backgroundColor: string = "transparent"
    @Input() selectedColor: string = "#3498db"
    @Input() leftLabelText: string = "Left"
    @Input() rightLabelText: string = "Right"
    @Input() leftLabelTextColor: string = "black"
    @Input() rightLabelTextColor: string = "white"
    @Input() fontSize: string = "15"
    @Input() alignment: string = "left"


    @Output() leftButton = new EventEmitter<any>();
    @Output() rightButton = new EventEmitter<any>();

    selectedLeftColor: string = this.selectedColor;
    selectedRightColor: string = "transparent";

    // leftButtonStyle: string = "clicked"
    // rightButtonStyle: string = "unclicked"

    constructor() {
    }

    public onLeftClick(args: EventData) {
        this.selectedLeftColor = this.selectedColor;
        this.selectedRightColor = "transparent"
        this.leftLabelTextColor = "black"
        this.rightLabelTextColor = "white"
        this.leftButton.emit(args)
    }

    public onRightClick(args: EventData) {
        this.selectedRightColor = this.selectedColor;
        this.leftLabelTextColor = "white"
        this.rightLabelTextColor = "black"
        this.selectedLeftColor = "transparent"
        this.rightButton.emit(args)
    }

}
