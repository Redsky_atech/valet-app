import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { SwitchButtonComponent } from "./components/switch-button.component";


@NgModule({
    imports: [NativeScriptCommonModule],
    declarations: [SwitchButtonComponent],
    schemas: [NO_ERRORS_SCHEMA],
    exports:[SwitchButtonComponent]
})

export class SwitchButtonModule { }
