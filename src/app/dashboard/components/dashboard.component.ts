import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Page, Observable } from "tns-core-modules/ui/page/page";
import { setCurrentOrientation } from "nativescript-screen-orientation";
import { TextField } from "tns-core-modules/ui/text-field";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { AgendaDetail } from "~/app/models/agenda-detail";
import { RouterExtensions } from "nativescript-angular/router";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { Booking } from "~/app/models/booking.model";
import { ListViewLinearLayout, RadListView, LoadOnDemandListViewEventData } from "nativescript-ui-listview";


var agendaDetail = [
  {
    time: "06:01",
    name: "Sumit",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n7989h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "165b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:02",
    name: "Abhimanyu",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n7989h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "165b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:03",
    name: "Isha",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n7889h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "165b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:04",
    name: "Sachin",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n7889h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "185b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:05",
    name: "Ravikant",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n8089h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "185b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:06",
    name: "Mahaveer",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n8189h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "195b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:07",
    name: "Saurabh",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n8189h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "205b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:08",
    name: "Sneh",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n8089h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "215b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:09",
    name: "Manisha",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n8289h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "215b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:10",
    name: "Manoj",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n8289h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "225b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:11",
    name: "Jaspreet",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n8389h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "235b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:12",
    name: "Jass",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n8489h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "235b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:13",
    name: "Gurpreet",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n8489h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "245b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:14",
    name: "Sunny",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n8589h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "255b8h",
    arrivalDeparture: "Arrival"
  },
  {
    time: "06:15",
    name: "New",
    email: "dsj.ds@gmail.com",
    mobile: "1234567890",
    carPlate: "n8689h",
    carModel: "Jewel Leannon V",
    carColor: "Black",
    flightNumber: "255b8h",
    arrivalDeparture: "Arrival"
  }
];


@Component({
  selector: "app-dashboard",
  moduleId: module.id,
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})

export class DashboardComponent implements OnInit {

  //RRad
  private _dataItems: ObservableArray<Booking>;
  private _sourceDataItems: ObservableArray<Booking>;
  private layout: ListViewLinearLayout;



  items = new ObservableArray();


  details = [];
  viewModel;
  date: string = "";
  customerName: string = "";
  carPlate: string = "";
  flightNumber: string = "";
  baseUrl: string = '';

  booking = true;
  constructor(private page: Page, private http: HttpClient, private routerExtensions: RouterExtensions, private _changeDetectionRef: ChangeDetectorRef) {
    this.page.actionBarHidden = true;
    setCurrentOrientation("landscape", function () {
      console.log("set landscape orientation");
    });

    this._sourceDataItems = new ObservableArray<Booking>();

    // for (let i = 0; i < agendaDetail.length; i++) {
    //   this.details.push(new AgendaDetail(agendaDetail[i]));
    // }
    // this.items.push("res://temp");
    // this.items.push("res://temp2");
    // this.items.push("res://temp3");
    // this.items.push("res://temp4");
  }

  ngOnInit(): void {

     this.onSearchTap();
    // this.layout = new ListViewLinearLayout();
    // this.layout.scrollDirection = "Vertical";
    // this.initDataItems();
    // this._changeDetectionRef.detectChanges();
    // this._dataItems = new ObservableArray<Booking>();
    // this.addMoreItemsFromSource(20);
    // // for (var i = 0; i < agendaDetail.length; i++) {
    // //   this.items.push(agendaDetail[i])
    // // }

  }


  private initDataItems() {
    this._sourceDataItems = new ObservableArray<Booking>();
    this.onSearchTap();

    // for (let i = 0; i < posts.names.length; i++) {
    //     if (androidApplication) {
    //         this._sourceDataItems.push(new DataItem(i, posts.names[i], "This is item description", posts.titles[i], posts.text[i], "res://" + posts.images[i].toLowerCase()));
    //     }
    //     else {
    //         this._sourceDataItems.push(new DataItem(i, posts.names[i], "This is item description", posts.titles[i], posts.text[i], "res://" + posts.images[i]));
    //     }
    // }
  }

  public nameTextField(args) {
    var customerName = <TextField>args.object;
    this.customerName = customerName.text;
    // if (this.customerName == "") {
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.pop();
    //   }
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.push(new AgendaDetail(agendaDetail[i]));
    //   }
    // }
  }

  public carPlateTextField(args) {
    var carPlate = <TextField>args.object;
    this.carPlate = carPlate.text;
    // if (this.carPlate == "") {
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.pop();
    //   }
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.push(new AgendaDetail(agendaDetail[i]));
    //   }
    // }
  }

  public flightNumberTextField(args) {
    var flightNumber = <TextField>args.object;
    this.flightNumber = flightNumber.text;
    // if (this.flightNumber == "") {
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.pop();
    //   }
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.push(new AgendaDetail(agendaDetail[i]));
    //   }
    // }
  }

  public dateTextField(args) {
    var date = <TextField>args.object;
    this.date = date.text;
    // if (this.flightNumber == "") {
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.pop();
    //   }
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.push(new AgendaDetail(agendaDetail[i]));
    //   }
    // }
  }


  // public get dataItems(): ObservableArray<Booking> {
  //   return this._sourceDataItems;
  // }

  // public addMoreItemsFromSource(chunkSize: number) {
  //   let newItems = this._sourceDataItems.splice(0, chunkSize);
  //   this.dataItems.push(newItems);
  // }

  // public onLoadMoreItemsRequested(args: LoadOnDemandListViewEventData) {
  //   const that = new WeakRef(this);
  //   const listView: RadListView = args.object;
  //   if (this._sourceDataItems.length > 0) {
  //     setTimeout(function () {
  //       that.get().addMoreItemsFromSource(20);
  //       listView.notifyLoadOnDemandFinished();
  //     }, 1500);
  //     args.returnValue = true;
  //   } else {
  //     args.returnValue = false;
  //     listView.notifyLoadOnDemandFinished(true);
  //   }
  // }


  public onItemTap(args) {

    if (this.details[args.index] != undefined) {
      let extendedNavigationExtras: ExtendedNavigationExtras = {
        queryParams: {
          "item": JSON.stringify(this.details[args.index])
        },
      };
      console.trace(this.details[args.index])

      this.routerExtensions.navigate(["/check"], extendedNavigationExtras)
    }

  }

  onSearchTap() {
    // console.log("search clicked");
    let customerNameSearch = this.customerName.toLowerCase();
    let carPlateSearch = this.carPlate.toLowerCase();
    let flightNumberSearch = this.flightNumber.toLowerCase();

    let query = '';


    if (customerNameSearch != '' && customerNameSearch != null && customerNameSearch != undefined) {
      query = query + "customer_name=" + customerNameSearch;
    }

    if (carPlateSearch != '' && carPlateSearch != null && carPlateSearch != undefined) {
      if (query != '') {
        query = query + "&" + "license_plate=" + carPlateSearch;
      }
      else {
        query = query + "license_plate=" + carPlateSearch;
      }
    }

    if (flightNumberSearch != '' && flightNumberSearch != null && flightNumberSearch != undefined) {
      if (query != '') {
        query = query + "&" + "flight_number=" + flightNumberSearch;
      }
      else {
        query = query + "flight_number=" + flightNumberSearch;
      }
    }

    if (this.date != '' && this.date != null && this.date != undefined) {
      if (query != '') {
        query = query + "&" + "date=" + this.date;
      }
      else {
        query = query + "date=" + this.date;
      }
    }

    if (query != '') {
      query = "?" + query
    }

    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, ""),
    });

    console.log("Query:", query)
    console.log("Token:", Values.readString(Values.AUTHERIZATION_TOKEN, ""))
    this.http.get(Values.BASE_URL + "/api/bookings" + query, { headers: headers }).subscribe((res: any) => {
      // this.items = res.data;

      console.trace(res)
      for (var i = 0; i < res.data.length; i++) {
        this._sourceDataItems.push(new Booking(res.data[i]));
      }

      // this.viewModel = new Observable();
      // this.viewModel.set("items", this.items);

    }, error => {

      console.trace(error)

    })
    // if (customerNameSearch !== "") {
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.pop();
    //   }
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     if (
    //       agendaDetail[i].name.toLowerCase().indexOf(customerNameSearch) !== -1
    //     ) {
    //       this.details.push(new AgendaDetail(agendaDetail[i]));
    //     }
    //   }
    // }
    // if (carPlateSearch !== "") {
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.pop();
    //   }
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     if (
    //       agendaDetail[i].carPlate.toLowerCase().indexOf(carPlateSearch) !== -1
    //     ) {
    //       this.details.push(new AgendaDetail(agendaDetail[i]));
    //     }
    //   }
    // }
    // if (flightNumberSearch !== "") {
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.pop();
    //   }
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     if (
    //       agendaDetail[i].flightNumber
    //         .toLowerCase()
    //         .indexOf(flightNumberSearch) !== -1
    //     ) {
    //       this.details.push(new AgendaDetail(agendaDetail[i]));
    //     }
    //   }
    // }
    // if (customerNameSearch !== "" && carPlateSearch !== "") {
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.pop();
    //   }
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     if (
    //       agendaDetail[i].name.toLowerCase().indexOf(customerNameSearch) !==
    //       -1 &&
    //       agendaDetail[i].carPlate.toLowerCase().indexOf(carPlateSearch) !== -1
    //     ) {
    //       this.details.push(new AgendaDetail(agendaDetail[i]));
    //     }
    //   }
    // }
    // if (
    //   customerNameSearch !== "" &&
    //   carPlateSearch !== "" &&
    //   flightNumberSearch !== ""
    // ) {
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.pop();
    //   }
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     if (
    //       agendaDetail[i].name.toLowerCase().indexOf(customerNameSearch) !==
    //       -1 &&
    //       agendaDetail[i].carPlate.toLowerCase().indexOf(carPlateSearch) !==
    //       -1 &&
    //       agendaDetail[i].flightNumber
    //         .toLowerCase()
    //         .indexOf(flightNumberSearch) !== -1
    //     ) {
    //       this.details.push(new AgendaDetail(agendaDetail[i]));
    //     }
    //   }
    // }
    // if (customerNameSearch !== "" && flightNumberSearch !== "") {
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.pop();
    //   }
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     if (
    //       agendaDetail[i].name.toLowerCase().indexOf(customerNameSearch) !==
    //       -1 &&
    //       agendaDetail[i].flightNumber
    //         .toLowerCase()
    //         .indexOf(flightNumberSearch) !== -1
    //     ) {
    //       this.details.push(new AgendaDetail(agendaDetail[i]));
    //     }
    //   }
    // }
    // if (carPlateSearch !== "" && flightNumberSearch !== "") {
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     this.details.pop();
    //   }
    //   for (let i = 0; i < agendaDetail.length; i++) {
    //     if (
    //       agendaDetail[i].carPlate.toLowerCase().indexOf(carPlateSearch) !==
    //       -1 &&
    //       agendaDetail[i].flightNumber
    //         .toLowerCase()
    //         .indexOf(flightNumberSearch) !== -1
    //     ) {
    //       this.details.push(new AgendaDetail(agendaDetail[i]));
    //     }
    //   }
    // }
  }
}
