import { NgModule, NO_ERRORS_SCHEMA, InjectionToken } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardComponent } from "./components/dashboard.component";
// import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular/listview-directives";


@NgModule({
  imports: [
    DashboardRoutingModule,
    NativeScriptFormsModule,
    NativeScriptHttpClientModule,
    // NativeScriptUIListViewModule
  ],
  declarations: [DashboardComponent],

  schemas: [NO_ERRORS_SCHEMA]
})
export class DashboardModule { }
