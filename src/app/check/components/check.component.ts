import { Component, ViewChild, ElementRef, AfterViewInit, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { fromNativeSource, fromBase64, ImageSource, fromResource } from "image-source";
import { registerElement } from "nativescript-angular/element-registry";
import { ActivatedRoute } from "@angular/router";
import { Mediafilepicker, ImagePickerOptions } from 'nativescript-mediafilepicker';
import { File } from "tns-core-modules/file-system/file-system";
import * as imagepicker from "nativescript-imagepicker";
import { isIOS, isAndroid } from "tns-core-modules/platform/platform";
import { ModalComponent } from "~/app/modals/modal.component";
import { Values } from "~/app/values/values";
import { Image } from "ui/image"
import { Page, Observable } from "tns-core-modules/ui/page/page";
import { Menu } from "nativescript-menu";
import { AgendaDetail } from "~/app/models/agenda-detail";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { Damage } from "~/app/models/damage.model";
import * as Toast from 'nativescript-toast';
import { Button } from 'ui/button/button'
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { listenToElementOutputs } from "@angular/core/src/view/element";
import { ListView, itemTemplateProperty } from "tns-core-modules/ui/list-view/list-view";
import { GestureService } from "~/app/gestures/gesture.services";

registerElement("DrawingPad", () => require("nativescript-drawingpad").DrawingPad);


declare var android: any;

class CarItem {

    baseString: string | ImageSource;
    showPlus: boolean;

    constructor(baseString: string | ImageSource, showPlus?: boolean) {
        this.baseString = baseString;
        this.showPlus = showPlus;
    }
}

class Agenda {

    time: string
    name: string
    email: string
    mobile: string
    carPlate: string
    carModel: string
    carColor: string
    flightNumber: string
    arrivalDeparture: string
}

@Component({
    selector: "check",
    moduleId: module.id,
    templateUrl: "./check.component.html",
    styleUrls: ["./check.component.css"]
})

export class CheckComponent implements OnInit, AfterViewInit {

    @ViewChild('selectionDialog') selectionModal: ModalComponent;
    @ViewChild('damageDialog') damageModal: ModalComponent;
    @ViewChild("signature") public drawingPad: ElementRef;
    @ViewChild("image") public imageRef: ElementRef;
    @ViewChild("damageList") public damageListRef: ElementRef;
    @ViewChild("button") public buttonRef: ElementRef;


    agendaDetail = [{ name: "res://valetlogo" }, { name: "res://valetlogo" }, { name: "res://valetlogo" }, { name: "res://valetlogo" }, { name: "res://valetlogo" }, { name: "res://valetlogo" }]
    actions = ["Add Car Damage Images", "Add Signature Images"];

    details;
    imageAssets = [];
    imageSrc: ImageSource;
    // src: any;
    img: Image;
    isSingleMode: boolean = true;
    thumbSize: number = 80;
    previewSize: number = 300;

    imageBaseString: string = "";
    padBaseString: string = "";
    isDrawing: boolean = true;
    canCheck: boolean;
    isBusy: boolean = false;

    pageIns: Page;

    private pad: any;

    item: Agenda;
    context;
    bookingID: string = "";
    checkingState: string = "checkin";

    isGetImageEnabled: boolean;
    isUploadEnabled: boolean;
    // filePath: string = "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20190204-WA0001.jpg";
    // file: File = File.fromPath(this.filePath)
    list = new ObservableArray();
    carImages: boolean;
    signature: boolean;
    otherServices: boolean;

    name: string = '';
    carPlate: string = '';
    private menu: Menu;

    positionText: string = '';
    button: Button;
    constructor(private page: Page, private activatedRoute: ActivatedRoute, private http: HttpClient, private activatedRouter: ActivatedRoute, private gestureService: GestureService) {
        this.activatedRoute.queryParams.subscribe(params => {
            this.item = JSON.parse(params["item"])
        });
        // this.list.push(new CarItem("res://car_damage", true));
        this.isDrawing = true;

        this.isGetImageEnabled = true;
        this.isUploadEnabled = true;
        this.page.actionBarHidden = true;

        this.carImages = false;
        this.signature = true;
        this.otherServices = false;

        this.canCheck = false;


        this.name = this.item.name;
        this.carPlate = this.item.carPlate;
        // console.log(this.item)

        this.details = [];
        for (let i = 0; i < this.agendaDetail.length; i++) {
            this.details.push(new AgendaDetail(this.agendaDetail[i]));
        }


    }

    // onPageLoaded(args) {
    //     this.pageIns = <Page>args.object;

    //     // this.pageIns = 
    // }

    public ngOnInit() {
        // let that = this;
    }


    public onDamageSubmit() {
        // this.isBusy = true;

        var that = this;
        // setTimeout(function () {
        //     that.isBusy = false;
        // }, 500)
        if (this.positionText != '') {
            // alert(this.positionText)
            var damageModel = new Damage();
            damageModel.position = this.positionText;
            // damageModel.position_type = this.;
            if (this.imageBaseString != null && this.imageBaseString != "") {
                damageModel.image_data = this.imageBaseString;
            }
            else {
                return;
            }


            let headers = new HttpHeaders({
                "Content-Type": "application/json",
                "Authorization": "Bearer " + Values.readString("AuthorizationToken", ""),
            });


            this.http.post(Values.BASE_URL + "/api/bookings/" + this.bookingID + "/damages", damageModel, { headers: headers }).subscribe((res: any) => {

                this.list.unshift(fromBase64(that.imageBaseString)); //use image string data from response
                console.log(res)

            }, error => {


            })
        }
        else {
            alert("Can not be Empty")
        }


        this.damageModal.hide();
    }

    public onTextChange(args) {
        var positionTextField = <TextField>args.object;
        this.positionText = positionTextField.text;
    }

    public openMenu() {
        Menu.popup({
            view: this.page.getViewById("menuButton"),
            actions: this.actions
        }).then(value => {
            // console.log(value)
            if (value == this.actions[0]) {
                // console.log(value == action[0]);
                this.carImages = true;
                this.signature = false;
                this.otherServices = false;
                this.selectionModal.show();
            }
            else if (value == this.actions[1]) {
                // console.log(value == action[1]);

                this.carImages = false;
                this.signature = true;
                this.otherServices = false;
                this.selectionModal.show();
            }
            else if (value == this.actions[2]) {
                // console.log(value == action[2]);

                this.carImages = false;
                this.signature = false;
                this.otherServices = true;
                this.isUploadEnabled = false;
            }
            // this.selectionModal.show();
            this.isGetImageEnabled = false;
        }).catch(console.log);
    }

    // public getImage() {
    //     this.selectionModal.show();
    //     this.isGetImageEnabled = false;
    //     this.isUploadEnabled = false;
    // }

    // public hideDialog() {
    //     this.selectionModal.hide();
    //     this.isGetImageEnabled = true;
    //     this.isUploadEnabled = true;
    // }


    public ngAfterViewInit() {
        this.pad = this.drawingPad.nativeElement;
        this.button = this.buttonRef.nativeElement as Button;
        console.log("But", this.button)
        this.gestureService.setGestureEvent(this.button, 'longPress');
        // this.img = this.imageRef.nativeElement;
        // const vm = new Observable();
        // vm.set("imageUri", "~/images/logo.png");
        // view.bindingContext = vm;
        // console.log(this.img)
        // while (this.isDrawing) {
        //     this.pad.getDrawing().then(data => {
        //         console.log("Res:", data);
        //         that.isUploadEnabled = true;
        //     }, error => {
        //         that.isUploadEnabled = false;
        //         console.log("Error:", error);
        //     });
        // }
    }

    // public onDrawingLoaded() {
    //     console.log("Draw")
    //     // setTimeout(() => {
    //     //     while (true) {
    //     //         this.getPadData();
    //     //         //    console.log("Draw")

    //     //     }
    //     // }, 1000)

    // }

    // public getPadData() {
    //     this.pad.getDrawing().then(data => {
    //         console.log("Res:", data);
    //         this.isUploadEnabled = true;
    //     }, error => {
    //         this.isUploadEnabled = false;
    //         console.log("Error:", error);
    //     });
    // }

    // public onDialogTap(dialogType: string) {
    //     console.log("Tapped")
    //     if (dialogType == "selectionModel") {
    //         console.log("TapChecked")
    //         this.selectionModal.hide();
    //     }
    //     else if (dialogType == "damageDialog") {
    //         console.log("TapChecked")
    //         this.damageModal.hide();
    //     }
    // }

    public captureImage() {
        let that = this;
        // that.isDrawing = false;
        that.selectionModal.hide();

        let options: ImagePickerOptions = {
            android: {
                isCaptureMood: true, // if true then camera will open directly.
                isNeedCamera: true,
                maxNumberFiles: 1,
                isNeedFolderList: false
            }, ios: {
                isCaptureMood: false, // if true then camera will open directly.
                maxNumberFiles: 1
            }
        };

        let mediafilepicker = new Mediafilepicker();
        mediafilepicker.openImagePicker(options);

        mediafilepicker.on("getFiles", function (res) {
            let results = res.object.get('results');
            console.log(results);
            if (results != null && results != undefined) {
                if (results.length != undefined && results != 0) {
                    let file = File.fromPath(results[0].file)
                    var baseString = that.getBase64String(file);
                    if (that.signature) {
                        that.padBaseString = baseString;

                        let img = that.imageRef.nativeElement as Image;
                        img.src = fromBase64(that.padBaseString);
                        let damageList = that.damageListRef.nativeElement as ListView;
                        if (damageList != undefined) {
                            damageList.refresh();
                        }
                        that.canCheck = true;
                        that.isDrawing = false;
                        // that.pad = this.drawingPad.nativeElement;
                    } else if (that.carImages) {
                        // that.damageModal.show();
                        // that.imageBaseString = that.getBase64String(file);
                        that.list.unshift(new CarItem(fromBase64(baseString)));
                    }
                }
            }
        });

        mediafilepicker.on("error", function (res) {
            let msg = res.object.get('msg');
            console.log(msg);
        });

        mediafilepicker.on("cancel", function (res) {
            let msg = res.object.get('msg');
            console.log(msg);
        });
    }


    public pickImage() {
        this.isDrawing = false;
        this.onSelectSingleTap();
    }

    public saveSignature() {
        let that = this;
        that.pad = that.drawingPad.nativeElement;
        if (this.isDrawing) {

            that.pad.getDrawing().then(data => {
                console.log("Res:", data);
                let image = fromNativeSource(data);
                that.updateImage(image);
                that.padBaseString = image.toBase64String("png");
                that.canCheck = true;
                // that.pad = that.drawingPad.nativeElement;
                // console.log(that.imageBaseString)
            }, error => {
                that.canCheck = false;
                console.log(that.pad);

                console.log("Error:", error);
                Toast.makeText("DrawingPad is Empty").show();
            });
        }
    }

    public updateImage(imageRes: ImageSource) {
        var img = this.imageRef.nativeElement as Image;
        img.src = imageRes;
    }

    public getSignatureAndUpload() {
        let that = this;
        that.pad = that.drawingPad.nativeElement;

        if (this.isDrawing) {
            that.pad.getDrawing().then(data => {
                console.log("Res:", data);
                let image = fromNativeSource(data);
                that.imageBaseString = image.toBase64String("png");
                let headers = new HttpHeaders({
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, ""),
                });

                let body = {
                    "signature": "data:image/png;base64," + that.imageBaseString
                };

                this.http.post(Values.BASE_URL + "/api/bookings/" + this.bookingID + "/" + this.checkingState, body, { headers: headers })
                    .subscribe((result: any) => {
                        console.log("res:", result)
                    }, error => {
                        console.log(error);
                    });
            }, error => {
                console.log("Error:", error);
            });
        }
    }

    public clearMyDrawing() {
        this.pad = this.drawingPad.nativeElement;

        this.pad.clearDrawing();
        this.canCheck = false;
        this.isDrawing = true;
        // this.isUploadEnabled = true;
        this.imageSrc = new ImageSource();
        // this.isUploadEnabled = false;
    }

    public onSelectMultipleTap() {
        this.isSingleMode = false;

        let context = imagepicker.create({
            mode: "multiple"
        });
        this.startSelection(context);
    }

    public onSelectSingleTap() {
        this.isSingleMode = true;

        let context = imagepicker.create({
            mode: "single"
        });
        this.startSelection(context);

    }

    private startSelection(context) {
        let that = this;
        that.selectionModal.hide();

        context
            .authorize()
            .then(() => {
                that.imageAssets = [];
                that.imageSrc = null;
                return context.present();
            })
            .then((selection) => {
                // console.log("Selection done: " + JSON.stringify(selection));
                // that.imageSrc = that.isSingleMode && selection.length > 0 ? selection[0] : null;
                if (selection[0] != null && selection[0] != undefined) {
                    var file = File.fromPath(selection[0]._android);
                    var baseString = that.getBase64String(file);
                    if (that.signature) {
                        that.padBaseString = baseString
                        var img = that.imageRef.nativeElement as Image;
                        img.src = fromBase64(that.padBaseString);
                        let damageList = that.damageListRef.nativeElement as ListView;
                        if (damageList != undefined) {
                            damageList.refresh();
                        }
                        that.canCheck = true;
                        that.isDrawing = true;
                    } else if (that.carImages) {
                        // that.damageModal.show();
                        // that.imageBaseString = that.getBase64String(file);
                        that.isDrawing = true;
                        that.list.unshift(new CarItem(fromBase64(baseString)));
                    }
                }
                else {
                    alert("No File Selected");
                }
                // console.trace(that.getBase64String(file))
                // that.src = fromBase64(that.getBase64String(file))
                that.imageAssets = selection;

            }).catch(function (e) {
                console.log(e);
            });
    }

    getImageThumb() {
        return fromBase64(this.imageBaseString);
    }


    public getBase64String(file: File) {
        const data = file.readSync();
        if (isIOS) {
            return data.base64EncodedStringWithOptions(0);
        }
        if (isAndroid) {
            return android.util.Base64.encodeToString(data, android.util.Base64.NO_WRAP);
        }
    }


}