import { NgModule, NO_ERRORS_SCHEMA, InjectionToken } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { CheckRoutingModule } from "./check-routing.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { CheckComponent } from "./components/check.component";
import { NgModalModule } from "../modals/ng-modal";
import { NativeScriptUIGaugeModule } from "nativescript-ui-gauge/angular/gauges-directives";
import { NgShadowModule } from 'nativescript-ng-shadow';
import { SwitchButtonModule } from "../switch-module/switch.module";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        CheckRoutingModule,
        NgModalModule,
        NativeScriptUIGaugeModule,
        NgShadowModule,
        SwitchButtonModule
    ],
    declarations: [CheckComponent],

    schemas: [NO_ERRORS_SCHEMA]
})
export class CheckModule { }
