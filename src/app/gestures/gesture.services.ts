import { Injectable } from '@angular/core';
import { View } from 'tns-core-modules/ui/page/page';
import { GestureTypes } from 'tns-core-modules/ui/gestures/gestures';
import { Color } from 'tns-core-modules/color/color';

@Injectable()
export class GestureService {



    constructor() {

    }

    public LightenDarkenColor(col, amt): string {

        var usePound = false;

        if (col[0] == "#") {
            col = col.slice(1);
            usePound = true;
        }

        var num = parseInt(col, 16);

        var r = (num >> 16) + amt;

        if (r > 255) r = 255;
        else if (r < 0) r = 0;

        var b = ((num >> 8) & 0x00FF) + amt;

        if (b > 255) b = 255;
        else if (b < 0) b = 0;

        var g = (num & 0x0000FF) + amt;

        if (g > 255) g = 255;
        else if (g < 0) g = 0;

        return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);

    }

    setGestureEvent(view: View, event: string | GestureTypes, animationType?: any) {
        var that = this;
        console.log(view)
        // console.log("Cols", view.backgroundColor)
        // var col = that.LightenDarkenColor(view.backgroundColor, -20)
        // console.log("after", col, typeof (col))

        view.on(event, function () {
            view.animate(
                {
                    backgroundColor: new Color("#696969"),
                    duration: 5000
                }
            )
        });
    }

}