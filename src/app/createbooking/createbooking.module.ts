import { NgModule, NO_ERRORS_SCHEMA, InjectionToken } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { CreateBookingComponent } from "./components/createbooking.component";
import { CreateRoutingModule } from "./createbooking-routing.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        CreateRoutingModule
    ],
    declarations: [CreateBookingComponent],

    schemas: [NO_ERRORS_SCHEMA]
})
export class CreateBookingModule { }
