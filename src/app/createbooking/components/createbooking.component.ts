import { Component } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { Arrival } from "~/app/models/arrival.model";
import { Departure } from "~/app/models/departure.model";
import { CarInfo } from "~/app/models/car-info.model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { Booking } from "~/app/models/booking.model";


@Component({
    selector: "app-create-booking",
    moduleId: module.id,
    templateUrl: "./createbooking.component.html",
    styleUrls: ["./createbooking.component.css"]
})

export class CreateBookingComponent {

    page: Page;

    bookingModel = new Booking();

    airportId: number;
    hasArrival: boolean;
    airlineIdDep: number;
    departureAt: string;
    flightNumberDep: string;
    airlineIdAri: number;
    arrivalAt: string;
    flightNumberAri: string;
    carBrandId: number;
    carColor: string;
    carLicencePlate: string;

    booking = true;

    constructor(private http: HttpClient) {

    }


    public airportIdTextField(args) {
        var textField = <TextField>args.object;
        this.airportId = parseInt(textField.text, 10);
    }

    public airlineIDDepTextField(args) {
        var textField = <TextField>args.object;
        this.airlineIdDep = parseInt(textField.text, 10);
    }

    public departureAtTextField(args) {
        var textField = <TextField>args.object;
        this.departureAt = textField.text;
    }

    public flightNumberDepTextField(args) {
        var textField = <TextField>args.object;
        this.flightNumberDep = textField.text;
    }

    public airlineIdAriTextField(args) {
        var textField = <TextField>args.object;
        this.airlineIdAri = parseInt(textField.text, 10);
    }

    public arrivalAtTextField(args) {
        var textField = <TextField>args.object;
        this.arrivalAt = textField.text;
    }

    public flightNumberAtTextField(args) {
        var textField = <TextField>args.object;
        this.flightNumberAri = textField.text;
    }

    public carBrandIdTextField(args) {
        var textField = <TextField>args.object;
        this.carBrandId = parseInt(textField.text, 10);
    }

    public carColorTextField(args) {
        var textField = <TextField>args.object;
        this.carColor = textField.text;
    }

    public carLicenceTextField(args) {
        var textField = <TextField>args.object;
        this.carLicencePlate = textField.text;
    }


    onPageLoaded(args) {
        this.page = args.object as Page;
    }

    onNextClick() {
        this.bookingModel = new Booking();
        let arrival = new Arrival();

        let departure = new Departure();
        let carInfo = new CarInfo();

        if (this.airportId != null && this.airportId != undefined) {
            this.bookingModel.airport_id = this.airportId;
        }

        if (this.hasArrival) {
            this.bookingModel.has_arrival = this.hasArrival;
        }

        if (this.airlineIdDep != undefined && this.airlineIdDep != null) {
            departure.airline_id = this.airlineIdDep;
        }

        if (this.departureAt != undefined && this.departureAt != null) {
            departure.departure_at = this.departureAt;
        }

        if (this.flightNumberDep != undefined && this.flightNumberDep != null) {
            departure.flight_number = this.flightNumberDep;
        }


        if (this.airlineIdAri != undefined && this.airlineIdAri != null) {
            arrival.airline_id = this.airlineIdAri;
        }

        if (this.arrivalAt != undefined && this.arrivalAt != null) {
            arrival.arrival_at = this.arrivalAt;
        }

        if (this.flightNumberAri != undefined && this.flightNumberAri != null) {
            arrival.flight_number = this.flightNumberAri;
        }

        if (this.carBrandId != null && this.carBrandId != undefined) {
            carInfo.car_brand_id = this.carBrandId;
        }

        if (this.carColor != null && this.carColor != undefined && this.carColor != "") {
            carInfo.color = this.carColor;
        }

        if (this.carLicencePlate != null && this.carLicencePlate != undefined && this.carLicencePlate != '') {
            carInfo.license_plate = this.carLicencePlate;
        }

        this.bookingModel.airport_id = this.airportId;
        this.bookingModel.has_arrival = this.hasArrival;
        this.bookingModel.arrival = arrival;
        this.bookingModel.departure = departure;
        this.bookingModel.carInfo = carInfo;
        this.booking = false;
    }


    onSubmitClick() {

        let headers = new HttpHeaders({
            "Authorization": Values.AUTHERIZATION_TOKEN
        })

        this.http.post(Values.BASE_URL + "/api/bookings", this.bookingModel, { headers: headers }).subscribe((res: any) => {
            if (res.data) {
                alert("Success")
                this.booking = false;
            }
        }, error => {
            alert(error)
            // alert("Error")
        })
    }

    onBackClick() {
        this.booking = true;
    }
}