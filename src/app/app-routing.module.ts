import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  { path: "login", loadChildren: "~/app/login/login.module#LoginModule" },
  { path: "dashboard", loadChildren: "~/app/dashboard/dashboard.module#DashboardModule" },
  { path: "check", loadChildren: "~/app/check/check.module#CheckModule" },
  // { path: "switch", component: SwitchButtonComponent }


];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
