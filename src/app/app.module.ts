import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { GridViewModule } from "nativescript-grid-view/angular"
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { GestureService } from "./gestures/gesture.services";
// import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular/listview-directives";


@NgModule({
  bootstrap: [AppComponent],
  imports: [NativeScriptModule, AppRoutingModule, HttpClientModule, GridViewModule, NativeScriptCommonModule],
  declarations: [AppComponent],
  providers: [GestureService],
  schemas: [NO_ERRORS_SCHEMA]
})


export class AppModule { }
