import { NO_ERRORS_SCHEMA, NgModule } from "@angular/core"
import { ModalComponent } from "./modal.component";
// import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular/side-drawer-directives";

// import { NativeScriptUIAutoCompleteTextViewModule } from "nativescript-ui-autocomplete/angular/autocomplete-directives";
import { NativeScriptUIGaugeModule } from "nativescript-ui-gauge/angular/gauges-directives";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";


@NgModule({
    imports: [
        // NativeScriptUISideDrawerModule,
        // NativeScriptUIAutoCompleteTextViewModule,
        NativeScriptUIGaugeModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule
    ],
    declarations: [
        ModalComponent,
    ],
    exports: [ModalComponent],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class NgModalModule { }