export class Damage {

    position: string;
    position_type: string;
    image_data: string;

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.position = obj.position;
        this.position_type = obj.position_type;
        this.image_data = obj.image_data;

    }
}