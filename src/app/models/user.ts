export class User {

  id: string;
  name: string;
  mobile_no: string;
  email: string;
  password: string;

  constructor(obj?: any) {
    if (!obj) {
      return;
    }
    this.email = obj.email;
    this.password = obj.password;
  }
}
