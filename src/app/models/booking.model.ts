import { Departure } from "./departure.model";
import { Arrival } from "./arrival.model";
import { CarInfo } from "./car-info.model";
import { User } from "./user.model";

export class Booking {

    airport_id: number;
    has_arrival: boolean;
    departure: Departure;
    arrival: Arrival;
    carInfo: CarInfo;
    user:User;
    // departure: Departure;

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.airport_id = obj.airport_id;
        this.has_arrival = obj.has_arrival;
        this.departure = obj.departure;
        this.arrival = obj.arrival;
        this.carInfo = obj.carInfo;
        this.user = obj.user;

    }
}