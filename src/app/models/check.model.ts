export class Check {

    signature: string;

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.signature = obj.signature;
    }
}