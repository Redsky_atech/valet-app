export class AgendaDetail {
  time: string;
  name: string;
  email: string;
  mobile: string;
  carPlate: string;
  carModel: string;
  carColor: string;
  flightNumber: string;
  arrivalDeparture: string;

  constructor(obj?: any) {
    if (!obj) {
      return;
    }
    
    this.time = obj.time;
    this.name = obj.name;
    this.email = obj.email;
    this.mobile = obj.mobile;
    this.carPlate = obj.carPlate;
    this.carModel = obj.carModel;
    this.carColor = obj.carColor;
    this.flightNumber = obj.flightNumber;
    this.arrivalDeparture = obj.arrivalDeparture;
  }
}
