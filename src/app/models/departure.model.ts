export class Departure {

    airline_id: number;
    departure_at: string;
    flight_number: string;

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.airline_id = obj.airline_id;
        this.departure_at = obj.departure_at;
        this.flight_number = obj.flight_number;
    }
}