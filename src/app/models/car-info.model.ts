export class CarInfo {

    car_brand_id: number;
    color: string;
    license_plate: string;

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.car_brand_id = obj.car_brand_id;
        this.color = obj.color;
        this.license_plate = obj.license_plate;
    }
}